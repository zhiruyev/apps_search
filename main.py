import os
import time
from uuid import uuid4

from elasticsearch import Elasticsearch
from sqlalchemy import Column, String, UUID, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from iin_generator import iin_generator
from readable_id import generate_human_readable_id

COUNT_ROWS = 100_000

database_uri = "postgresql://apps_search:apps_search@localhost:5433/apps_search"
elastic_url = "http://localhost:9200"


"""Preparing elastic"""
es = Elasticsearch([elastic_url], http_auth=('elastic', "'[[z\*=YdY'9"))
mappings = {
    "properties": {
        "uuid": {"type": "text"},
        "iin": {"type": "text"},
        "readable_id": {"type": "text"}
    }
}
es.indices.create(index="applications", mappings=mappings)
print("Elastic index created")


"""Preparing PostgreSQL"""
engine = create_engine(database_uri)
Base = declarative_base()


class Applications(Base):
    __tablename__ = 'applications'

    uuid = Column(UUID, primary_key=True)
    iin = Column(String, nullable=False)
    readable_id = Column(String, nullable=False)


Base.metadata.create_all(engine)
print("SQL table created")


"""Inserting data"""
Session = sessionmaker(bind=engine)
session = Session()
saved_readable_id = ''

for i in range(COUNT_ROWS):
    uuid_, iin, readable_id = str(uuid4()), iin_generator(), generate_human_readable_id()
    if i == COUNT_ROWS / 2:
        saved_readable_id = readable_id

    doc = {
        "uuid": uuid_,
        "iin": iin,
        "readable_id": readable_id,
    }
    new_application = Applications(
        uuid=uuid_,
        iin=iin,
        readable_id=readable_id,
    )
    session.add(new_application)
    es.index(index="applications", id=str(i), document=doc)
    print(f'Inserted {i}th row')
    os.system('clear')

session.commit()
print("Data inserted into elastic and postgres")


"""Benchmark PostgreSQL"""
for i in range(5):
    print(f"PostgreSQL {i}th benchmark")
    start = time.time()
    result = session.query(Applications).filter_by(readable_id=saved_readable_id).first()
    end = time.time()
    total = end - start
    print(f'PostgreSQL performed search by {total:.4f} s')
session.close()

"""Benchmark Elasticsearch"""
query = {
    "query": {
        "match": {
            "readable_id.keyword": saved_readable_id
        }
    }
}
for i in range(5):
    print(f"Elastic {i}th benchmark")
    start = time.time()
    results = es.search(index='applications', body=query)
    end = time.time()
    total = end - start
    print(f'Elasticsearch performed search by {total:.4f} с')

import string
import random
from datetime import datetime


class HumanReadableIdGenerator:

    MIN_YEAR = 2022
    MAX_YEAR = 2049

    def generate(self) -> str:
        """
        EXAMPLE: ABCD-12345678
        where A - indicates current year
            B - month
            C - day
            D - hour
            12 - min
            34 - sec
            567 - mil
            8 - random digit
        """
        return self._generate()

    def _generate(self):
        today = datetime.now()
        year_letter = self.get_current_year_letter()
        month_letter = string.ascii_uppercase[today.month]
        day_letter = self.get_current_day_letter()
        hour_letter = self.get_current_hour_letter()
        minute = self.get_current_minute_digit()
        second = self.get_current_second_digit()
        microsecond = self.get_current_microsecond_digit()
        random_digit = self.get_random_digit()

        return f"{year_letter}{month_letter}{day_letter}{hour_letter}-{minute}{second}{microsecond}{random_digit}"

    def get_current_year_letter(self) -> str:
        today = datetime.now()
        if today.year < self.MIN_YEAR or today.year >= self.MAX_YEAR:
            raise NotImplementedError()

        return string.ascii_uppercase[today.year-self.MIN_YEAR]

    def get_current_day_letter(self) -> str:
        day = datetime.now().day
        res = []

        al = string.ascii_uppercase

        while day > 0:
            day -= 1
            res.append(al[(day % 26) ])
            day //= 26

        res.reverse()
        return "".join(res)

    def get_current_hour_letter(self) -> str:
        current_hour = datetime.now().hour
        return string.ascii_uppercase[current_hour]

    def get_current_minute_digit(self) -> str:
        today = datetime.now()
        minute = str(today.minute)
        if len(minute) != 2:
            minute = "0" + minute
        return minute

    def get_current_second_digit(self) -> str:
        today = datetime.now()
        second = str(today.second)
        if len(second) != 2:
            second = "0" + second
        return second

    def get_current_microsecond_digit(self) -> str:
        today = datetime.now()
        microsecond = str(today.microsecond%1000)
        if len(microsecond) != 3:
            microsecond = "0" + microsecond
        return microsecond

    def get_random_digit(self) -> str:
        return str(random.randint(0, 9))


def generate_human_readable_id() -> str:
    return HumanReadableIdGenerator().generate()

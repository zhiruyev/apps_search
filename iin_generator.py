import random
from datetime import date


def iin_generator():
    start_date = date.today().replace(year=1950, month=1, day=1).toordinal()
    end_date = date.today().replace(year=1999, month=1, day=1).toordinal()
    random_day = date.fromordinal(random.randint(start_date, end_date))
    day_str = random_day.strftime("%y%m%d")
    century = random.choice([2, 3])  # sex and century

    just_random = random.randint(1000, 9999)
    iin = f"{day_str}{century}{just_random}"

    c_sum_1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
    c_sum_2 = [3, 4, 5, 6, 7, 8, 9, 10, 11, 1, 2]

    checksum = sum([int(i) * v for i, v in zip(iin, c_sum_1)]) % 11

    if checksum == 10:
        checksum = sum([int(i) * v for i, v in zip(iin, c_sum_2)]) % 11

    iin = f"{iin}{checksum}"
    if len(iin) != 12:
        iin = iin_generator()

    return iin
